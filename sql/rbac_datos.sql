-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-04-2020 a las 02:26:38
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rbac`
--

--
-- Volcado de datos para la tabla `desempenia`
--

INSERT INTO `desempenia` (`id`, `usuario_id`, `rol_id`, `created_at`) VALUES
(1, 1, 1, '2020-04-03 00:24:38'),
(2, 2, 2, '2020-04-03 00:24:38');

--
-- Volcado de datos para la tabla `obtiene`
--

INSERT INTO `obtiene` (`id`, `rol_id`, `permiso_id`, `created_at`) VALUES
(1, 1, 2, '2020-04-03 00:22:58'),
(2, 1, 1, '2020-04-03 00:22:58'),
(3, 2, 1, '2020-04-03 00:23:09');

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`id`, `nombre`, `descripcion`, `created_at`) VALUES
(1, 'ver', NULL, '2020-04-03 00:20:33'),
(2, 'registrar', NULL, '2020-04-03 00:20:33');

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `descripcion`, `created_at`) VALUES
(1, 'laboratorista', NULL, '2020-04-03 00:21:05'),
(2, 'usuario_registrado', NULL, '2020-04-03 00:21:05');

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `password`, `nombre`, `created_at`) VALUES
(1, 'hulk', 'hulkhulk', 'Bruce Banner', '2020-04-03 00:24:18'),
(2, 'lalo', 'lalolalo', 'Eduardo Juárez', '2020-04-03 00:24:18');
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
