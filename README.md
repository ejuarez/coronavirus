# Coronavirus App #

Coronavirus App: Un aplicación ejemplo para las sesiones de clase de Desarrollo de aplicaciones web 

### ¿Para qué es este repositorio? ###

* En este repositorio se irán poniendo los ejemplos de cada una de las sesiones de clase
* 0.2.1

### Infraestructura ###

#### Servidor de base de datos ####

* Versión del servidor de bases de datos: 10.4.6-MariaDB

#### Servidor web ####

* Apache/2.4.41 (Win64) OpenSSL/1.1.1c PHP/7.3.9
* Versión del cliente de base de datos: libmysql - mysqlnd 5.0.12-dev 
* extensión PHP: mysqli

### Equipo de profesores ###

* Eduardo Juárez (edjuarezp@tec.mx)
* Ricardo Cortés (rcortese@tec.mx)